package com.example.hellojni;

public class HelloJni {
	static {
		System.loadLibrary("qvis");
	}
	public static native String stringFromJNI();
}
