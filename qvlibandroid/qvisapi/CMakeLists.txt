# Sets the minimum version of CMake required to build your native library.
# This ensures that a certain set of CMake features is available to
# your build.

cmake_minimum_required(VERSION 3.4.1)

# Specifies a library name, specifies whether the library is STATIC or
# SHARED, and provides relative paths to the source code. You can
# define multiple libraries by adding multiple add.library() commands,
# and CMake builds them for you. When you build your app, Gradle
# automatically packages shared libraries with your APK.


file(GLOB_RECURSE qvis_SRC

    "../TestNative/*.h"
    "../TestNative/*.c"
    "../TestNative/*.cpp"

)

add_library( # Specifies the name of the library.
             qvis

             # Sets the library as a shared library.
             SHARED

             # Provides a relative path to your source file(s).
             ${qvis_SRC} )


#add_custom_command(TARGET native-lib POST_BUILD
#        COMMAND ${CMAKE_COMMAND} -E copy
#        ${CMAKE_SOURCE_DIR}/../QVSO/qualvision/libs/libNewAllStreamParser.so
#        ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/libNewAllStreamParser.so
#    )

# Specifies a path to native header files.
include_directories(../TestNative/)

target_link_libraries(qvis android)