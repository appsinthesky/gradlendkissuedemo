/*
 * nvr.h
 *
 *  Created on: 11 Apr 2017
 *      Author: tom
 */

#ifndef NVR_H_
#define NVR_H_

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include "platform.h"
#include "cortex/net_if.h"
#include "cortex/cmd_if.h"
#include "cortex/cmd_int.h"
#include "xm/xm.h"

#ifdef __cplusplus
#define NVRCDECL extern "C"
#else
#define NVRCDECL
#endif

#define CAMERA_NAME_LEN 64
typedef struct _nvr_camera {
	unsigned int number;			//number of the camera
	char name[CAMERA_NAME_LEN];		//name of the camera
	char connection;				//connection type, see CAMERA_CONNECTION_XXX below
	char visible;					//1 = is visible, 0 = cannot be shown in lists
	char connected;					//1 = is connected
	char pan_tilt_enabled;			//1 = can pan and tilt
	char zoom_enabled;				//1 = can zoom
	char playback_enabled;			//1 = can play back
	char audio_enabled;				//1 = supports audio
	unsigned short playback_fps;	//FPS for playback
	unsigned short live_fps;		//FPS for live monitor
} nvr_camera;

#define CAMERA_CONNECTION_ANALOGUE	0
#define CAMERA_CONNECTION_IP		1

typedef enum {
	XM,
	CORTEX,
#ifdef WITH_TVT
	TVT = 2,
#endif
#ifdef WITH_QUALVISION
	QUALVISION = 3,
#endif
} nvr_session_type;

typedef struct _nvr_session {
	nvr_session_type session_type;
	int timezone_offset_secs;
	void *session;
	char *ip;
	char *p2p_id;
	unsigned short port;
	char *username;
/*	int num_cameras;
	nvr_camera *cameras;*/
} nvr_session;

#define INVALID_TIMEZONE_OFFSET  	(-1000000)	//invalid offset: minus 1m seconds

typedef enum {
	UNKNOWN,
	H264,
	H265
} nvr_protocol;

typedef struct _nvr_stream {
	nvr_session *session;				//pointer to the user login session that this stream uses
	int stream_id;						//if the NVR provides an ID number for reference
	char stream_type;					//playback or live (defined below)
	int camera_id;						//ID number (not index!!) of camera
    char camera_name[CAMERA_NAME_LEN];	//name of camera (Cortex requires this)
	int resolution;						//resolution as defined by Cortex
	nvr_protocol protocol;				//protocol
	void *stream;						//pointer to an structure required by the NVR SDK
} nvr_stream;

typedef struct _nvr_frame {
	void *framedata;					//pointer to the frame data
	size_t framelen;					//length of data from the above
	nvr_protocol protocol;				//H.264 or H.265 data?
} nvr_frame;

#define PLAYSPEED_PAUSE		0
#define PLAYSPEED_FWD_DIV16	6
#define PLAYSPEED_FWD_DIV8		12
#define PLAYSPEED_FWD_DIV4		25
#define PLAYSPEED_FWD_DIV2		50
#define PLAYSPEED_FWD_x1		100
#define PLAYSPEED_FWD_x2		200
#define PLAYSPEED_FWD_x4		400
#define PLAYSPEED_FWD_x8		800
#define PLAYSPEED_FWD_x16		1600
#define PLAYSPEED_FWD_x32		3200
#define PLAYSPEED_FWD_x64		6400
#define PLAYSPEED_FWD_x128		12800
#define PLAYSPEED_FWD_x256		25600
#define PLAYSPEED_REW_x1		-100
#define PLAYSPEED_REW_x2		-200
#define PLAYSPEED_REW_x4		-400
#define PLAYSPEED_REW_x8		-800
#define PLAYSPEED_REW_x16		-1600
#define PLAYSPEED_REW_x32		-3200
#define PLAYSPEED_REW_x64		-6400
#define PLAYSPEED_REW_x128		-12800
#define PLAYSPEED_REW_x256		-25600

/* Values for the time_meaning parameter of nvr_configure_stream.
 * The difference between ABSOLUTE and ABSOLUTE_FROM_NVR is that Cortex machines need a value which is
 * a standard Unix epoch, minus the NVR's timezone. So if we're being passed an absolute time, we need
 * to remove the timezone difference ourselves. If however we're passing a time which came from the NVR
 * itself, then we should not remove the difference. This happens for example when receiving time info
 * from push notifications.
 *
 * ABSOLUTE therefore adjusts as above, and ABSOLUTE_FROM_NVR does not.
 */

#define TIME_MEANING_NO_CHANGE			-1
#define TIME_MEANING_RELATIVE			0
#define TIME_MEANING_BEGINNNING			1
#define TIME_MEANING_END				2
#define TIME_MEANING_ABSOLUTE			3
#define TIME_MEANING_ABSOLUTE_FROM_NVR	4

#define MOTOR_MOVEMENT_PAN_LEFT			0
#define MOTOR_MOVEMENT_PAN_RIGHT		1
#define MOTOR_MOVEMENT_TILT_UP			2
#define MOTOR_MOVEMENT_TILT_DOWN		3
#define MOTOR_MOVEMENT_ZOOM_IN			4
#define MOTOR_MOVEMENT_ZOOM_OUT			5
#define MOTOR_MOVEMENT_FOCUS_NEAR		6
#define MOTOR_MOVEMENT_FOCUS_FAR		7
#define MOTOR_MOVEMENT_IRIS_OPEN		8
#define MOTOR_MOVEMENT_IRIS_CLOSE		9

#define STREAM_TYPE_LIVE				0
#define STREAM_TYPE_PLAYBACK			1

#define RESOLUTION_QCIF					0
#define RESOLUTION_CIF					1
#define RESOLUTION_2CIF					2
#define RESOLUTION_D1					3
#define RESOLUTION_4CIF					4
#define RESOLUTION_720P					10
#define RESOLUTION_1080P				11

#define CAPABILITY_GET_SERVER_VER		0
#define CAPABILITY_GET_DVR_NAME_AND_ID	1
#define CAPABILITY_GET_THUMBNAIL		2

NVRCDECL int nvr_init_once();
NVRCDECL int nvr_get_capability(nvr_session_type session_type, int capability);
NVRCDECL int nvr_get_server_version(nvr_session_type session_type, const char *ip_address, const char *port_number, const char *p2p_id, int *ver_major, int *ver_minor, int *ver_bugfix);
NVRCDECL int nvr_login(nvr_session *session, nvr_session_type session_type, const char *ip_address, unsigned short port_number, const char *p2p_id, const char *username, const char *password);
NVRCDECL int nvr_guess_specific_io_error(nvr_session *session);
NVRCDECL int nvr_logout(nvr_session *session, int *was_logged_in);
NVRCDECL int nvr_disconnect(nvr_session *session);
NVRCDECL int nvr_get_dvr_name_and_id(nvr_session *session, char **name, char **unique_id);
NVRCDECL int nvr_get_camera_info(nvr_session *session, int *num_cameras, nvr_camera **cameras);
NVRCDECL int nvr_open_stream(nvr_session *session, nvr_stream **stream, int camera_id, const char *camera_name, int res, char stream_type);
NVRCDECL int nvr_configure_stream(nvr_stream *stream, int speed, long long int time, int time_meaning);
NVRCDECL int nvr_change_stream_resolution(nvr_stream *stream, int resolution);
NVRCDECL int nvr_close_stream(nvr_stream *stream);
NVRCDECL int nvr_get_frame(nvr_stream *stream, nvr_frame *frame);
NVRCDECL int nvr_update_timezone(nvr_session *session);
NVRCDECL int nvr_get_thumbnail(nvr_session *session, int camera_id, const char *camera_name, void **frame, size_t *frame_len);
NVRCDECL int nvr_move_motor(nvr_session *session, int camera_id, int motor_movement, int start);
NVRCDECL int nvr_register_push_notify(nvr_session *session, const char *token);
NVRCDECL int nvr_unregister_push_notify(nvr_session *session, const char *token);

#endif /* NVR_H_ */
