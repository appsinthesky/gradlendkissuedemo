package com.example.qvtester;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.hellojni.HelloJni;

public class TesterActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tester);

		TextView text = findViewById(R.id.test_text);
		text.setText("JNI string: "+ HelloJni.stringFromJNI());
	}
}
